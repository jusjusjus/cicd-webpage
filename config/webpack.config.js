"use strict";

const path = require('path');

const base_dir = path.resolve(path.join(__dirname, '..'));
const source_dir = path.resolve(path.join(base_dir, 'web'));
const build_dir = path.resolve(path.join(base_dir, 'build'));

console.log('%%%%%%% Webpack base_dir   =', base_dir);
console.log('%%%%%%%         source_dir =', source_dir);
console.log('%%%%%%%         build_dir  =', build_dir);
console.log('%%%%%%%');

module.exports = {
  resolve: {
    extensions: [ '.js'],
    modules: [
      path.resolve(base_dir, 'node_modules'),
      path.resolve(base_dir, 'code')
    ],
  },
  mode: 'development',
  entry: path.join(source_dir, 'index.js'),
  output: {
    path: build_dir,
    filename: 'index_bundle.js',
  },
  target: "web",
  externals: {},
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000
  }
};
