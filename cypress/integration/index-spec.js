
describe('index spec', () => {
  it('Opens index.html', () => {
    cy.visit('web/index.html');
    cy.contains("Hello world");
  });
});
