
class GreetingComponent extends HTMLElement {

  connectedCallback () {
    const p = document.createElement('p');
    p.innerHTML = "Hello world";
    this.appendChild(p);
  }

}

module.exports = GreetingComponent;
